PDFLATEX	=	pdflatex
OPTIONS		=	-synctex=1 -interaction=nonstopmode
CLEAN_EXT	=	aux|bbl|bcf|blg|chktex|fdb_latexmk|fls|lacheck|log|out|pdf|run.xml|synctex.gz|toc|zip

build: pdf tableofcontents

pdf tableofcontents:
	$(PDFLATEX) $(OPTIONS) main.tex

.PHONY : build clean

clean:
	- find . -regextype posix-egrep -regex ".*\.($(CLEAN_EXT))" -type f -delete