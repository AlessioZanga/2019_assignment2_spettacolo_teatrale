# 2019 Assignment 2 - Spettacolo Teatrale

# 0. Introduzione

## The Coccia Experience
#### Vivi un'esperienza incredibile e immersiva nei panni della Società dei Palchettisti alle prese con uno scottante mistero.
<br/>
![img](./img/coccia-650x390.jpg)

## Il Progetto
Un'associazione ludica del novarese (i cultisti anonimi) necessita di una piattaforma web per sponsorizzare i propri eventi e gestire le prenotazioni agli stessi. Tali eventi prevedono la partecipazione interattiva del pubblico rendendo loro stessi i protagonisti di una rappresentazione basata sulla scrittura creativa e sul teatro d'improvvisazione, il progetto infatti si pone come una forma di Live Action Role Play (LARP) fortemente incentrato sull'investigazione.<br/><br/>
Nel caso di "The Coccia Experience", i partecipanti si immedesimeranno nelle figure dei palchettisti, figure storiche le cui famiglie hanno gestito il teatro fin dalla sua nascita, per rivivere le atmosfere dei primi del '900 colme di intrighi e contrapposizioni tra nobiltà e borghesia. Il tutto alla scoperta della storia del teatro Coccia di Novara e delle sue location nascoste, comprese quelle normalmente chiuse al pubblico.<br/><br/>
Ai giocatori verrà fornita una scheda con le caratteristiche del proprio personaggio come carattere, background e mire per il futuro; tutto il resto potranno inventarlo e personalizzarlo a proprio piacimento. In questa sorta di canovaccio saranno anche indicati degli obiettivi, pensati con un ordine crescente di impegno e immedesimazione richiesto per raggiungerli.<br/><br/>
La piattaforma web avrà il compito di mostrare la facciata dell'associazione e di esporre questo progetto,  permettendo inoltre la prenotazione di un dato evento tramite la selezione di uno di personaggi ancora disponibili.<br/><br/>

## Incipit
Novara, 1919<br/>
L'ambiente del teatro è da sempre avvolto da un velo di superstizione, il Coccia non è da meno. Da anni infatti gira voce di un fantasma che abita le sue mura ed è causa di rumori e incidenti misteriosi. Negli ultimi mesi questi incidenti sono aumentati drasticamente e sono culminati, due giorni fa, con la morte di Eleonora Prina, prima ballerina del balletto in cartellone: *"La Sylphide"*.<br/><br/>
Questo scandalo potrebbe decretare la fine del teatro, dunque la società dei Palchettisti decide di riunirsi per trovare una soluzione.
